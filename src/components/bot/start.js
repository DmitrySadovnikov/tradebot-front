import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

class BotStart extends Component {

  constructor (props) {
    super(props);
    this.state = {
      date:    moment.utc(),
      minutes: 1440,
    };
    this.handleChangeDate    = this.handleChangeDate.bind(this);
    this.handleChangeMinutes = this.handleChangeMinutes.bind(this);
    this.handleSubmit        = this.handleSubmit.bind(this);
  }

  handleChangeDate(date) {
    this.setState({
      date:    date,
      minutes: this.state.minutes,
    });
  }

  handleChangeMinutes(e) {
    this.setState({
      date:    this.state.date,
      minutes: e.target.value,
    });
  }

  timeConverter(timestamp) {
    var date = timestamp.date() + '.' + (timestamp.month() + 1) + '.' + timestamp.year();
    return date
  }

  handleSubmit(e) {
    e.preventDefault();

    fetch('http://localhost:3333/bots/start', {
      method:  'PATCH',
      headers: {
        Accept:         'application/json',
        'Content-Type': 'application/json',
      },
      body:    JSON.stringify({
        date:    this.state.date,
        minutes: this.state.minutes,
      }),
    })
      .then(response => {
        if (response.ok) {
          console.log('ok');
          window.location.search = 'date=' + this.timeConverter(this.state.date);
        }
        else {
          console.log('fail');
        }
      });
  }

  render() {
    return <div className={ 'col-xs-12' }>
      <form onSubmit={ this.handleSubmit }>
        <div className={ 'form-group ' } style={ { display: 'inline-flex' } }>
          <input
            name='minutes'
            type="text"
            value={ this.state.minutes }
            onChange={ this.handleChangeMinutes }
          />
          <DatePicker
            selected={ this.state.date }
            onChange={ this.handleChangeDate }
            dateFormat="D.M.Y"
            name='date'
            className={ 'form-control mb-2' }
          />
          <input type="submit" value="Bot Start"
                 className={ 'btn btn-primary mb-2' }/>
        </div>
      </form>
    </div>;
  }
}

export default BotStart;
