import React, { Component } from 'react';

class OrderIndex extends Component {

  constructor(props) {
    super(props);
    this.state = {
      collection: [],
    };
  }

  componentDidMount() {
    const readingUrl = 'http://localhost:3333/orders' + window.location.search;
    fetch(readingUrl)
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            this.setState({
              collection: json.collection,
            });
          });
        }
        else {
          console.log('fail');
        }
      });
  }

  handleDelete(e) {
    e.preventDefault();

    fetch('http://localhost:3333/orders/destroy_all', {
      method:  'PATCH',
      headers: {
        Accept:         'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.ok) {
          console.log('ok');
          window.location.reload()
        }
        else {
          console.log('fail');
        }
      });
  }

  render() {
    return (
      <div className={ 'col-xs-6' } style={ { textAlign: 'left' } }>
        <table className={ 'table table-striped' }>
          <thead>
          <tr>
            <th>status</th>
            <th>created_at</th>
            <th>executed_at</th>
            <th>cancelled_at</th>
            <th>price</th>
            <th>original_amount</th>
            <th>remaining_amount</th>
          </tr>
          </thead>
          <tbody>
          {
            this.state.collection.map((item) => {
              return (
                <tr
                  key={ item.id }
                  style={ { color: item.side === 'buy' ? '#00cc00' : '#ff0000' } }>
                  <td>{ item.status }</td>
                  <td>{ item.created_at }</td>
                  <td>{ item.executed_at }</td>
                  <td>{ item.cancelled_at }</td>
                  <td>{ item.price }</td>
                  <td>{ item.original_amount }</td>
                  <td>{ item.remaining_amount }</td>
                </tr>
              );
            })
          }
          </tbody>
        </table>

        <form onSubmit={this.handleDelete}>
          <button
            type="Submit"
            className={ 'btn btn-primary mb-2' }>
            Delete all
          </button>
        </form>
      </div>
    );
  }
}

export default OrderIndex;
