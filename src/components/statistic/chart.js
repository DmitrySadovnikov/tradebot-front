import React, { Component } from 'react';
import {
  LineChart,
  Line,
  CartesianGrid,
  Tooltip,
  XAxis,
  YAxis,
  Legend,
} from 'recharts';

class StatisticChart extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const readingUrl = 'http://localhost:3333/statistics/orders_by_time' + window.location.search;

    fetch(readingUrl)
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            this.setState({
              data: json.collection,
            });
          });
        }
      });
  }

  render() {
    return (
      <LineChart width={ 1600 } height={ 800 } data={ this.state.data }
                 margin={ { top: 5, right: 30, left: 20, bottom: 5 } }>
        <XAxis dataKey="time"/>
        <YAxis dataKey="price" orientation="left"
               domain={ ['dataMin', 'dataMax'] }/>
        <CartesianGrid stroke="#eee" strokeDasharray="5 5"/>
        <Tooltip/>
        <Legend/>
        <Line type="monotone" dataKey="price" stroke="#c1c1c1" strokeDasharray="5 5"
              activeDot={ false } dot={false}/>
        <Line type="monotone" dataKey="buy" stroke="#00cc00" fill="#00cc00"
              activeDot={ { r: 5 } }/>
        <Line type="monotone" dataKey="sell" stroke="#ff0000" fill="#ff0000"
              activeDot={ { r: 5 } }/>
        <Line type="monotone" dataKey="bid" stroke="#00cc00"
              activeDot={ { r: 5 } }/>
        <Line type="monotone" dataKey="ask" stroke="#ff0000"
              activeDot={ { r: 5 } }/>
        <Line type="monotone" dataKey="cancell_bid" stroke="#4c684c"
              activeDot={ { r: 5 } }/>
        <Line type="monotone" dataKey="cancell_ask" stroke="#894d4d"
              activeDot={ { r: 5 } }/>
        <Line type="monotone" dataKey="short_period_ema" stroke="#00cc00"
              activeDot={ false } dot={false}/>
        <Line type="monotone" dataKey="long_period_ema" stroke="#ff0000"
              activeDot={ false } dot={false}/>
      </LineChart>
    );
  }
}

export default StatisticChart;
