import React, { Component } from 'react';

class BalanceIndex extends Component {

  constructor(props) {
    super(props);
    this.state = {
      collection: [],
    };
  }

  componentDidMount() {
    fetch('http://localhost:3333/balances')
      .then(response => {
        if (response.ok) {
          response.json().then(json => {
            this.setState({
              collection: json.collection,
            });
          });
        }
      });
  }

  handleReset(e) {
    e.preventDefault();

    fetch('http://localhost:3333/balances/reset', {
      method:  'PATCH',
      headers: {
        Accept:         'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        if (response.ok) {
          console.log('ok');
          window.location.reload()
        }
        else {
          console.log('fail');
        }
      });
  }

  render() {
    return (
      <div className={ 'col-xs-6' } style={ { textAlign: 'left' } }>
        <table className={ 'table table-striped' }>
          <thead>
          <tr>
            <th>currency</th>
            <th>amount</th>
            <th>available</th>
          </tr>
          </thead>
          <tbody>
          {
            this.state.collection.map((item) => {
              return (
                <tr key={ item.id }>
                  <td>{ item.currency }</td>
                  <td>{ item.amount }</td>
                  <td>{ item.available }</td>
                </tr>
              );
            })
          }
          </tbody>
        </table>

        <form onSubmit={this.handleReset}>
          <button type="Submit" className={ 'btn btn-primary mb-2' }>Reset</button>
        </form>
      </div>
    );
  }
}

export default BalanceIndex;
