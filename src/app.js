import React, { Component } from 'react';
import './app.css';
import StatisticChart from './components/statistic/chart';
import BalanceIndex from './components/balance/index';
import OrderIndex from './components/order/index';
import BotStart from './components/bot/start';

class App extends Component {
  render() {
    return (
      <div className="App">
        <StatisticChart />
        <BotStart />
        <BalanceIndex />
        <OrderIndex />
      </div>
    );
  }
}

export default App;
